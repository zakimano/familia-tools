#!/usr/bin/python3

import sys
# import fileinput
import re


def main():
	
	if len(sys.argv) == 3:
		fin = sys.argv[1]
		fout = sys.argv[2]
		pass
	else:
		try:
			print("[ -!- ]\tPélda a bemeneti fájl egy sorára:\n[ -!- ]\tVezeték Keresztnév\temail@cim.com" +
			"\n[ -!- ]\tA sorrend felcserélhető, az elválasztás tabulátorral történik." +
			"\n[ --- ]")
			fin = input("[ -?- ]\tAdd meg a bemeneti fájl nevet:\t")
			fout = input("[ -?- ]\tAdd meg a kimeneti fájl nevet:\t")
			pass
		except Exception as e:
			print("[ !!! ]\tHiba!\n" + str(e))
			exit()
		pass
	
	print("[ ... ]\tFájl megnyitása: " + str(fin))
	
	# Bemeneti fájl megnyitása
	try:
		f = open(fin, encoding="utf-8")
		pass
	except Exception as e:
		fin = (str(fin) + ".txt")
		print("[ -!- ]\tFájl megnyitása sikertelen, fájlnév megváltoztatva: " + str(fin))
		try:
			f = open(fin, encoding="utf-8")
			pass
		except Exception as e:
			print("[ !!! ]\tHiba!\n[ -!- ]\tFájlmegnyitás sikertelen.")
			exit()
			pass
		pass
	
	print("[ ... ]\tFájl olvasása: " + str(fin))
	
	# Olvasás
	lines = f.readlines()
	
	# Bemeneti fájl bezárása
	f.close()
	
	print("[ ... ]\tFájl feldolgozása: " + str(fin) + "\n[ -!- ]\tKimeneti fájl: " + str(fout))
	
	if not re.match("([^\";,<>])+.(txt|text)", fout):
		fout = (str(fout) + ".txt")
		print("[ -!- ]\tKimeneti fájlnév módosítva: " + str(fout))
		pass
	
	# Kimeneti fájl megnyitása
	with open(fout, "w", encoding="utf-8") as fileout:
		
		i = 1
		
		# Sorok vizsgálata
		for line in lines:
			
			# Általános eset
			if re.match("([^\";,<>])+\t([^\";,<>])+", line):
				
				barray = line.split("\t")
				
				if "@" in barray[1]:
					fileout.write("\"" + barray[0].strip() + "\" <" + barray[1].strip() + ">,\n")
					pass
				else:
					fileout.write("\"" + barray[1].strip() + "\" <" + barray[0].strip() + ">,\n")
					pass
					
				pass
			
			# Kész esetek
			# Helyes
			elif re.match("\"([^@])+\" <([.\-_a-zA-Z0-9@])+>,", line):
				fileout.write(line.strip() + "\n")
				pass
			# Helytelen
			elif re.match("\"([^@])+\"\t<([.\-_a-zA-Z0-9@])+>,", line):
				barray = line.split("\t")
				fileout.write(barray[0].strip() + " " + barray[1].strip() + "\n")
				pass
			
			# Számozás kihagyása
			elif re.match("--- ([0-9])+ ---", line):
				i -= 1
				pass
			
			# Üres, túl rövid sorok kihagyása
			elif len(line) < 2:
				i -= 1
				pass
			
			# Hibás eset
			else:
				fileout.write(" - HIBA:\t" + line.strip() + "\n")
				pass
			
			# Számlálás
			if ((i % 10) == 0 and i != 0):
				fileout.write("--- " + str(i) + " ---\n")
				pass
			
			i += 1
			
			pass
		
		pass


main()
