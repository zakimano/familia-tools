#!/usr/bin/python3

import sys
# import fileinput
import re


def main():
	
	if len(sys.argv) == 3:
		fin = sys.argv[1]
		fout = sys.argv[2]
		pass
	else:
		exit()
		pass
	
	print("Fájl olvasása: " + str(fin))
	
	# Bemeneti fájl megnyitása
	with open(fin) as f:
		# Olvasás
		lines = f.readlines()
	
	print("Fájl feldolgozása: " + str(fin) + "\nKimeneti fájl: " + str(fout))
	
	# Kimeneti fájl megnyitása
	with open(fout, "w") as fileout:
		
		i = 1
		carr = []
		
		# Sorok vizsgálata
		for line in lines:
			
			# Általános eset
			if re.match("([^\";,<>])+\t([^\";,<>])+", line):
				
				barray = line.split("\t")
				
				if "@" in barray[1]:
					fileout.write("\"" + barray[0].strip() + "\" <" + barray[1].strip() + ">,\n")
					pass
				else:
					fileout.write("\"" + barray[1].strip() + "\" <" + barray[0].strip() + ">,\n")
					pass
					
				pass
			
			# Kész esetek
			# Helyes
			elif re.match("\"([^@])+\" <([.\-_a-zA-Z0-9@])+>,", line):
				
				curl = re.search("<(.+?)>", line)
				if curl.group(1) not in carr:
					fileout.write(line.strip() + "\n")
					pass
				carr.append(curl.group(1))
				
				pass
			# Helytelen
			elif re.match("\"([^@])+\"\t<([.\-_a-zA-Z0-9@])+>,", line):
				barray = line.split("\t")
				fileout.write(barray[0].strip() + " " + barray[1].strip() + "\n")
				pass
			
			# Számozás kihagyása
			elif re.match("--- ([0-9])+ ---", line):
				i -= 1
				pass
			
			# Hibás eset
			else:
				fileout.write(" - HIBA:\t" + line.strip() + "\n")
				pass
			
			# Számlálás
			if (i % 10) == 0:
				fileout.write("--- " + str(i) + " ---\n")
				pass
			
			i += 1
			
			pass
		
		pass


main()
